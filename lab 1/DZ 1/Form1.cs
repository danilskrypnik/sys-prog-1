﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DZ_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
        /*
        
        Задание №1
Написать программу, которая отображает информацию в
ListView об активных процессах в системе. Автоматически
обновляет список процессов. Должно отображаться: имя
процесса, ID, количество потоков и открытых дескрипторов.
Возможность сохранить информацию с ListView в файл.

        */
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Timer timer = new Timer();
            timer.Interval = 1000;
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            listView1.Columns.Clear();
            listView1.Items.Clear();

            listView1.Columns.Add("Id");
            listView1.Columns.Add("Process name");
            listView1.Columns.Add("Threads count");
            listView1.Columns.Add("Opened descriptors");

            listView1.View = View.Details;

            foreach (Process proc in Process.GetProcesses())
            {
                var item = new ListViewItem();

                item.Text = proc.Id.ToString();
                item.SubItems.Add(proc.ProcessName);
                item.SubItems.Add(proc.Threads.Count.ToString());
                item.SubItems.Add(proc.HandleCount.ToString());

                listView1.Items.Add(item);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var sr = new StreamWriter("processes_info.txt"))
            {
                foreach (ListViewItem row in listView1.Items)
                {
                    string info = "Id: " + row.SubItems[0].Text
                        + " ProcessName: " + row.SubItems[1].Text
                        + " Threads count: " + row.SubItems[2].Text
                        + " Opened descriptors count: " + row.SubItems[3].Text;

                    sr.WriteLine(info);
                }
            }
        }
    }
}
