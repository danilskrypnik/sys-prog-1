﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

namespace Sysprog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            /*
            Задание №1
            Написать программу, которая при нажатии кнопки “Процессы” выводит в ListView информацию об активных процессах в
            системе. Минимум должно отображаться имя процесса и ID.

            Задание №2
            Доработать предыдущее задание. Добавить на форму 2 элемнета для вывода информации о выбранном в ListView процессе. В одном элементе выводить информацию о потоках процесса (идентификатор, приоритет, время запуска), а во втором информацию о модулях процесса (название и путь к модулю)
            */
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView1.Columns.Clear();

            listView1.Columns.Add("id");
            listView1.Columns.Add("name");
            listView1.View = View.Details;


            try
            {
                foreach (Process proc in Process.GetProcesses())
                {
                    ListViewItem item = new ListViewItem();

                    item.Text = proc.Id.ToString();
                    item.SubItems.Add(proc.ProcessName);

                    listView1.Items.Add(item);
                }
            }
            catch (Exception) { }
        }


        private void listView1_Click(object sender, EventArgs e)
        {
            try
            { 
                var row = listView1.SelectedItems[0];
                int proc_id = int.Parse(row.SubItems[0].Text);
                var proc = Process.GetProcessById(proc_id);

                var procinfo = new StringBuilder();
                var moduleinfo = new StringBuilder();

                foreach (ProcessThread thread in proc.Threads)
                    procinfo.AppendLine($"Id: {thread.Id}; Priority: {thread.PriorityLevel}; Start time: {thread.StartTime}");

                foreach (ProcessModule item in proc.Modules)
                    moduleinfo.AppendLine(item.ModuleName + " : " + item.FileName);

                labelThreads.Text = procinfo.ToString();
                labelModule.Text = moduleinfo.ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
