﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary
{
    enum MartialStatus
    {
        Single, Married
    }
    public class Person
    {
        string firstname;
        string surname;
        int age;
        MartialStatus martialStatus;

        public Person(string firstname, string surname, int age)
        {
            this.firstname = firstname;
            this.surname = surname;
            this.age = age;
            this.martialStatus = MartialStatus.Single;
        }

        public string Print()
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Firstname: {this.firstname}");
            builder.AppendLine($"Surname: {this.surname}");
            builder.AppendLine($"Age: {this.age}");

            return builder.ToString();
        }
    }
}
