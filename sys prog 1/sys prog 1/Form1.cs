﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace sys_prog_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            process1.StartInfo = new ProcessStartInfo("calc.exe");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            process1.CloseMainWindow();
            process1.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            process1.Start();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo("calc.exe");
            process.Start();
            MessageBox.Show("calc PID = " + process.Id);

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(process.ProcessName);

            process.WaitForExit();
            richTextBox1.Text = builder.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var builder = new StringBuilder();
            var name    = AssemblyName.GetAssemblyName("SampleLibrary.dll"); 
            var asm     = Assembly.Load(name); // assembly includes modules
            var module  = asm.GetModule("SampleLibrary.dll");

            foreach (Type type in module.GetTypes()) // all types in module
                builder.AppendLine(type.Name);

            Type Person = module.GetType("SampleLibrary.Person"); // [modulename].[type]
            object p = Activator.CreateInstance(Person, new object[] { "Daniel", "Skrypnik", 19, }); // create instance of some class (type, constructor params)
            string str = (string)Person.GetMethod("Print").Invoke(p, null); // Print - is a method of Person.cs class. Invoke reciece object and his parameters

            builder.AppendLine(str);

            richTextBox1.Text = builder.ToString();
        }
    }
}
